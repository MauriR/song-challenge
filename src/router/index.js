import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home'
import MainPanel from '../views/MainPanel'

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/panel',
    name: 'Panel',
    component: MainPanel
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
