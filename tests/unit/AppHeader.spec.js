import AppHeader from "../../src/components/AppHeader"
import { mount } from '@vue/test-utils'


describe('AppHeader', () => {
  xtest(' If user is not logged in, do not show the Logout button ', () => {
    const wrapper = mount(AppHeader)

    expect(wrapper.find('button').isVisible()).toBe(false)
  })

  xtest(' If user is logged in, show the Logout button ', async () => {
    const wrapper = mount(AppHeader)
    wrapper.setData({ loggedIn: true })
    await wrapper.vm.$nextTick()

    expect(wrapper.find('button').isVisible()).toBe(true)
  })
})


//.find()
//.trigger() para que haga click y tal