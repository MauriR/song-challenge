import SongCard from "../../src/components/SongCard"
import { mount } from '@vue/test-utils'

describe('songCard', ()=>{
// xtest('input', async()=>{
//   const wrapper = mount(SongCard)
//   wrapper.setData({ answer:'Correct Answer', isAnswerCorrect: true })
//   await wrapper.vm.$nextTick()
//   expect(wrapper.find('.song-input')).toBe(true)
// })

test('Answers', async()=>{
  const wrapper = mount(SongCard)
  await wrapper.setValue({ song:'Correct Answer', correctSong: 'Correct Answer' })
  
  expect(wrapper.vm.song).toBe('Correct Answer')
})
})